export interface IField {
    name: string;
    icon?: string;
    required?: boolean;
    title?: string;
    placeholder?: string;
    description?: string;
    group?: string;
    fieldType?: string;
    systemName?: string;
    column?: number;
}

export const defaultFields: IField[] = [{
    name: "EMail",
    icon: "mail",
    required: true,
    title: "Поле Email",
    placeholder: "Email",
    group: "Элементы данных",
    systemName: "email",
    column: 3,
}, {
    name: "Имя",
    icon: "title",
    title: "Поле имя",
    placeholder: "Имя",
    group: "Элементы данных",
    systemName: "name",
    column: 3,
}, {
    name: "Телефон",
    icon: "phone",
    required: true,
    title: "Поле Телефон",
    placeholder: "Телефон",
    group: "Элементы данных",
    systemName: "phone",
    column: 3,
}, {
    name: "Подписка",
    required: true,
    fieldType: "submit",
    icon: "check",
    group: "Кнопки",
    title: "Подписаться",
    column: 3,
}, {
    name: "Подтверждение",
    fieldType: "confirm",
    icon: "check_box",
    systemName: "confirmation",
    group: "Элементы данных",
    title: "Я соглашаюсь с условиями обработки персональных данных",
    column: 1,
}];

export const Widget = {
    code: `<LpPopup class="myclass">232
    <LpImage data="leftimage"/>
    <div class="LP-content">
        <LpHtml data="title" class="LP-title"/>
        <LpHtml data="description" class="LP-description"/>
        <LpForm/>
    </div>
    <LpClose/>
</LpPopup>
`,
    model: {
        title: "Подпишитесь на нашу рассылку!",
        description: "Зарегистрируйтесь,чтобы получать последние новости, обновления и специальные предложения.",
        note: "Я соглашаюсь с условиями обработки персональных данных",
        button: "Подписаться",
        leftimage: "",
        checkbox: "true",
        form: defaultFields,
    },
    less: `.LP-wrapper {
    top: 20px;
    left: 20px;
    right: 20px;
    bottom: 65px;
    position: fixed;
    display: flex;
    flex-wrap: wrap;
    justify-content: extract(@position, 2);
    align-items: extract(@position, 1);
    pointer-events: none;
  * {
      pointer-events: all;
  }
  &.LP-mobile {
    justify-content: center;
    align-items: flex-start;
    .LP-background {
      background: rgba(0,0,0,0.5);
      position: fixed;
      top: 0;
      bottom: 0;
      width: 100%;
    }
  }
}
p {
  margin: 0;
}
.LP-col-1 { width: 100%; }
.LP-col-2 { width: 50%; }
.LP-col-3 { width: 33%; }
.LP-ek-description {
    font-size: small;
}

.LP-popup {
  display: none;
  border-radius: 6px;
  box-shadow: 0 5px 40px rgba(0, 0, 0, .16);
  background: #ffffff;
  color: #444444;
  position: relative;
}
.LP-popup.LP-show {
    display: block;
}
.LP-image {
  display: table-cell;
  vertical-align:middle;
  padding: 0;
  line-height:0;
}
.LP-image img {
  border-radius: 0px;
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.LP-content {
  padding: 24px;
  display: table-cell;
  padding: 20px;
  vertical-align:middle;
  max-width: 450px;
}
.LP-title {
  color: #222222;
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 16px;
}
.LP-description {
  color: #444444;
  font-size: 15px;
  margin-bottom: 16px;
  line-height: 1.4;
}
.LP-form
{
  display: table;
  width: 100%;
}
.LP-input-block {
  display: inline-block;
  padding-right: 5px;
}
.LP-input-label {
  width: 15px;
  height: 15px;
  background: #ddd;
  margin-bottom: -3px;
  border-radius: 3px;
  position: relative;
  cursor: pointer;
  display: table-cell;
}
.LP-checkbox-label {
  display: table-cell;
  font-size: 12px;
  font-weight: 400;
}
.LP-input {
  border: 1px solid transparent;
  transition: border .2s ease-out;
  background-color: #f5f5f5;
  font-size: 14px;
  padding: 0 8px ;
  color: #444444;
  box-sizing: border-box;
  height: 32px;
  width: 100%;
  text-align: left;
  border-radius: 3px;
  display: block;
}
.LP-checkbox {
    width: 13px;
    height: 13px;
    margin: 0;
}
.LP-submit-block {
  display: table-cell;
  vertical-align: top;
}
.LP-submit {
  display: block;
  color: #ffffff;
  background: #2780e3;
  font-size: 14px;
  line-height: 32px;
  padding: 0 30px;
  border-style: none;
  border-radius: 3px;
  cursor: pointer;
  transition: background-color .2s ease-out;
  font-weight: bold;
  white-space: nowrap;
}
.LP-submit:hover {
  background: #106dd4;
}
.LP-submit:disabled {
  background: #e9e9e9;
  cursor:none;
  pointer-events:none;
}
.LP-incorrect {
  box-shadow: none !important;
  border: 1px solid #2780e3 !important;
}
.LP-note {
  line-height: 1.4;
  opacity: .6;
  margin-top: 16px;
  font-size: 12px;
  white-space: normal;
  overflow-wrap: break-word;
  word-wrap: break-word;
  word-break: normal;
  display: table;
}
.LP-conditions__mark-title {
  color: #444444;
  font-size: 12px;
  font-weight: normal !important;
}
.LP-close-x {
  position: absolute;
  display: inline-block;
  width: 25px;
  height: 25px;
  overflow: hidden;
  right: 12px;
  top: 14px;
  cursor: pointer;
}
.LP-close-x::before,
.LP-close-x::after {
  content: '';
  position: absolute;
  height: 1px;
  width: 100%;
  top: 50%;
  left: 0;
  margin-top: -1px;
  background: #222222;
}
.LP-close-x::before {
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
}
.LP-close-x::after {
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
}
.LP-mobile .LP-image {
 display:none;
}
.LP-mobile .LP-input-block {
    display: block;
    padding-bottom: 7px;
    padding-right: 0px;
}
.LP-mobile .LP-note .LP-input-block {
  display: table-cell;
  padding-right: 5px !important;
}
.LP-mobile .LP-submit {
  margin-top: 10px;
  width: 100%;
  height: 40px;
}
.LP-mobile .LP-submit-block {
    display: block !important;
}
.LP-mobile .LP-description {
  font-size: 14px;
}`,
};

export default Widget;

