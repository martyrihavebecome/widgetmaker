import tsc, { ModuleKind, ScriptTarget, JsxEmit } from "typescript";

export default class Compiler {
    public static compile(code: string, options?: tsc.CompilerOptions): string {
        return tsc.transpileModule(code, {
            fileName: "class.tsx",
            compilerOptions: {
                ...options,
                jsx: JsxEmit.React,
                jsxFactory: "h",
                sourceMap: false,
                target: ScriptTarget.ES5,
                module: ModuleKind.None,
            },
        }).outputText;
    }
}
