import BaseComponent from "./components/BaseComponent";
import LpField from "./components/LpField";
import LpForm from "./components/LpForm";
import LpHtml from "./components/LpHtml";
import LpImage from "./components/LpImage";
import LpNote from "./components/LpNote";
import LpClose from "./components/LpClose";
import LpPopup from "./components/LpPopup";
import LpSubmit from "./components/LpSubmit";
import LpSuccess from "./components/LpSuccess";
import Inner from "./components/Inner";
import JSXCompiler from "./JsxCompiler";

export { BaseComponent, LpField, LpForm, LpHtml, LpImage, LpNote, LpClose, LpPopup, LpSubmit, LpSuccess, JSXCompiler, Inner };

export interface Page {
    id?: string;
    name?: string;
    code: string;
    model: object;
    success?: boolean;
    createNode?: any;
}
