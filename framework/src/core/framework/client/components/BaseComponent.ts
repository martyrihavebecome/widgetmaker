import { JSXCompiler } from "./../index";
import { Component } from "vue";
import Operations, { Operation } from "../utils/operations";
import { ClientHttp2Session } from "http2";

export interface Event { name: string; fn: CallBack; }

export interface Props {
    [prop: string]: any;
    class?: string;
    children?: Array<BaseComponent<Props>>;
    data?: string;
    style?: CSSStyleDeclaration | string;
    show?: boolean;
    key?: string;
    text?: string;
    inner?: string;
    events?: Event[];
    pageId?: string;
}

export interface IComponent {
    tag: string;
    props: object;
    children?: IComponent[];
}

type CallBack = (...args: any[]) => any;

interface Events {
    [name: string]: CallBack[];
}

export const lpSymbol = "lp_component";


// tslint:disable-next-line: max-classes-per-file
export default class BaseComponent<T extends Props> implements JSX.Element, IComponent {
    public static defaultProps = {};
    public static dataProp: string;
    public element!: Node;
    public tag!: string;
    public children?: Array<BaseComponent<Props>>;
    public cls!: string;
    public keys: string[];
    public dynamicEvents!: Operations<Event & { key: string }>;
    public dynamicClasses!: Operations<{ name: string, key: string }>;
    constructor(public props: T & Props = {} as T) { }

    public $createElement(cls: typeof BaseComponent | string, props: object, ...children: any[]): BaseComponent<any> {
        return JSXCompiler.$createElement(cls, props, children);
    }

    public render(h: any): any { return null; }

    public addEventListener(name: string, fn: CallBack): void {
        const dynamicEvents = this.dynamicEvents || (this.dynamicEvents = new Operations());
        dynamicEvents.add(new Operation({ name, fn, key: "add" },
            () => this.element.addEventListener(name, fn)), (data) => data.name === name && data.fn === fn && data.key === "remove", true);
    }

    public removeEventListener(name: string, fn: CallBack): void {
        const dynamicEvents = this.dynamicEvents || (this.dynamicEvents = new Operations());
        dynamicEvents.add(new Operation({ name, fn, key: "remove" },
            () => this.element.removeEventListener(name, fn)), (data) => data.name === name && data.fn === fn && data.key === "add", true);
    }

    public addClass(name: string): void {
        const dynamicClasses = this.dynamicClasses || (this.dynamicClasses = new Operations());
        dynamicClasses.add(new Operation({ name, key: "add" },
            () => (this.element as HTMLElement).classList.add(name)), (data) => data.name === name && data.key === "remove", true);
    }

    public removeClass(name: string): void {
        const dynamicClasses = this.dynamicClasses || (this.dynamicClasses = new Operations());
        dynamicClasses.add(new Operation({ name, key: "remove" },
            () => (this.element as HTMLElement).classList.remove(name)), (data) => data.name === name && data.key === "add", true);
    }

    public querySelectorAll(selector: string): Component | null {
        const element = this.element;
        return element instanceof HTMLElement ? Array.prototype.map.call(element.querySelectorAll(selector), (node) => node[lpSymbol]) : [];
    }

    public initDynamics(): void {
        const { dynamicClasses, dynamicEvents } = this;
        if (dynamicClasses) { dynamicClasses.execute(); }
        if (dynamicEvents) { dynamicEvents.execute(); }
    }

    public find<P extends Props, C extends BaseComponent<P>>(callBack: (cmp: C) => boolean): C[] {
        const children = this.children;
        const res = [];
        if (children) {
            for (const child of children) {
                if (child) {
                    if (callBack(child as C)) {
                        res.push(child);
                    } else {
                        res.push.apply(res, child.find(callBack));
                    }
                }
            }
        }
        return res;
    }

    public foreach(callback: (cmp: BaseComponent<Props>) => void): void {
        const children = this.children;
        callback(this);
        if (children) {
            for (const child of children) {
                if (child) { child.foreach(callback); }
            }
        }
    }
}

export function defineProps<T>(obj1: T, obj2: Partial<T>): T {
    if (obj1) {
        Object.keys(obj2).forEach((prop: string) =>
            Object.defineProperty(obj1, prop, { enumerable: false, value: (obj2 as any)[prop] }));
    }
    return obj1;
}

