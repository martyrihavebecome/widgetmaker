import LpPage from "./LpPage";

export default class LpSuccess extends LpPage<{ show?: boolean }> {
    public render(h?: any) {
        return <div class={"LP-popup LP-success" + (this.props.show ? " LP-show" : "")}>
            {this.props.children}
        </div>;
    }
}
