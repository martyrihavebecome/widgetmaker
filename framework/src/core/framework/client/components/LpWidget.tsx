import BaseComponent, { Props } from "./BaseComponent";
import CompilationContext from "./CompilationContext";

export interface Page {
    code: string;
    success?: boolean;
    model: object;
}


export default class LpWidget extends BaseComponent<{}> {
    public tag = "LpWidget";
    constructor(public pages: Page[], public context: object) { super(); }

    public render(h?: any) {
        const pages = this.pages;
        const context = this.context;
        const compilationContexts = pages.map((page) => new CompilationContext(page.success ? { ...page, code: pages[0].code } : page, context, { show: "0" }));
        // TODO: Подумать над оптимизацией
        return <div class="LP-wrapper">
            {[<div class="LP-background"></div>].concat(compilationContexts.map((compilationContext) => compilationContext.render()))}
        </div>;
    }
}
