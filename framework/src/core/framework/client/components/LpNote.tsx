import BaseComponent from "./BaseComponent";
import { Inner } from "..";

export interface NoteProps {
    title?: string;
}

// @data
export default class LpNote extends BaseComponent<NoteProps> {
    public render(h?: any) {
        return this.props.title ? <div class="LP-note">
            <div class="LP-input-block"><input class="LP-checkbox" id="LP-conditions-mark-1"
                type="checkbox" name="LP-conditions-mark-1"
                placeholder="" value="" />
                <Inner text={this.props.title} />
                {/* {this.props.title} */}
            </div>
        </div> : null;
    }
}
