import BaseComponent from "./BaseComponent";
export interface ISubmitProps { title?: string; }

// @data
export default class LpSubmit extends BaseComponent<ISubmitProps> {

    public render(h?: any) {
        if (this.props.title) {
            return <div class="LP-input-block">
                <button type="submit" class="LP-submit">{this.props.title}</button>
            </div>;
        } else {
            return null;
        }
    }
}
