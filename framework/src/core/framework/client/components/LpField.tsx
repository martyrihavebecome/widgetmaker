import BaseComponent from "./BaseComponent";

export default class LpField extends BaseComponent<any> {
    public render(h?: (...args: any[]) => any): any {
        const { name, description, title, systemName } = this.props;
        return name ? <div class={["LP-input-block", this.props.class,
            `LP-el-${systemName && systemName.toLowerCase()}`].join(" ")}>
            {title && <div class="Lp-el-title">{title}</div>}
            {description && <div class="Lp-el-description">{description}</div>}
            <input type="text" class="LP-input" placeholder={this.props.placeholder} value=""></input>
        </div> : null;
    }
}

