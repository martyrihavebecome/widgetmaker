import BaseComponent, { Props } from "./BaseComponent";

import * as components from "..";
import { JSXCompiler } from "..";
import LpPage from "./LpPage";

interface CompilationProps extends components.Page {
    context: object;
    emit: (id: string, action: string, data: any) => void;
}

export interface SystemContext {
    show?: string;
}

const cmpRegexp = /LP-\w{4,}/g;

export default class CompilationContext {
    private static componentHash: object = null;
    private static applyModel(cls: typeof BaseComponent | string, model: any, props: Props): object {
        if (typeof cls !== "string") {
            const defaultProps = cls.defaultProps;
            if (defaultProps) {
                props = { ...defaultProps, ...props };
            }
        }
        if (props) {
            const result = {};
            if (typeof cls !== "string") {
                const dataProp = cls.dataProp;
                if (props.data !== void 0 && dataProp) {
                    const value = props.data;
                    result[dataProp] = model[value];
                }
            }

            Object.keys(props).forEach((key) => {
                let value = props[key];
                if (typeof value === "string" && value.startsWith("@")) {
                    value = value.substr(1);
                    result[key] = model[value];
                } else {
                    result[key] = value;
                }
            });
            return result;
        }
        return props;
    }

    private static getComponents() {
        return this.componentHash || (this.componentHash = Object.keys(components).reduce((res: object, key: string) => (res[key.replace("Lp", "lp-").toLowerCase()] = components[key], res), Object.create(null)));
    }

    constructor(public page: components.Page, public context: object, public systemContext: SystemContext) { }

    public $createElement(cls: typeof BaseComponent | string, props: Props, ...children: any[]): BaseComponent<any> {
        const { model, success, id } = this.page;
        if (!props) { props = {}; }
        if (props.class) {
            const foundedClasses = props.class.match(cmpRegexp);
            if (foundedClasses) {
                const componentHash = CompilationContext.getComponents();
                foundedClasses.forEach((foundedClsName) => {
                    const foundedCls = componentHash[foundedClsName.toLowerCase()];
                    if (foundedCls) {
                        cls = foundedCls;
                    }
                });
            }
        }
        props.pageId = id;
        if (success) {
            if (cls === components.LpPopup) {
                cls = components.LpSuccess as typeof BaseComponent;
                props.show = this.systemContext.show === "success";
            }
        }
        if (cls === components.LpPopup) { props.show = this.systemContext.show !== "success"; }
        if (cls === components.LpForm) {
            if (success) {
                return null;
            } else {
                props.onsubmit = (e: MouseEvent) => {
                    this.systemContext.show = "success";
                    e.preventDefault();
                };
            }
        }
        props = CompilationContext.applyModel(cls, model, props);
        const result = JSXCompiler.$createElement(cls, props, children, this.$createElement.bind(this));
        return result;
    }

    public render(): any {
        const context = { h: this.$createElement.bind(this), ...this.context };
        const contextKeys = Object.keys(context);

        const factory = Function.apply(null, Object.keys(context).concat(this.page.code));
        return factory.apply(this, contextKeys.map((key) => context[key])) as BaseComponent<Props>;
    }
}
