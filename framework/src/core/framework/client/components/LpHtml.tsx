import BaseComponent from "./BaseComponent";

// @data
export default class LpHtml extends BaseComponent<{ text: string }> {
    public static dataProp = "text";
    public render(h?: any) {
        return this.props.text ? <div inner={this.props.text}></div> : null;
    }
}

