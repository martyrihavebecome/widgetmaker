import BaseComponent, { Props } from "./BaseComponent";
import LpField from "./LpField";
import LpSubmit from "./LpSubmit";
import LpNote from "./LpNote";


interface FormProps {
    fields: any[];
}


// @data
export default class LpForm extends BaseComponent<FormProps> {
    public static dataProp = "fields";
    public static defaultProps = {
        data: "form",
    };


    public resolveField(h: any, field: any) {
        if (field.fieldType === "submit") {
            return <LpSubmit {...field} class={`LP-col-${field.column}`}></LpSubmit>;
        } else if (field.fieldType === "confirm") {
            return <LpNote {...field} class={`LP-col-${field.column}`}></LpNote>;
        } else {
            return <LpField {...field} class={`LP-col-${field.column}`}></LpField>;
        }
    }

    public render(h: any): JSX.Element {
        const fields = this.props.fields || [];
        return <form class="LP-form">{this.children ||
            <div class="form-content">
                {fields.map((field) => this.resolveField(h, field))}
            </div>}
        </form>;
    }
}

