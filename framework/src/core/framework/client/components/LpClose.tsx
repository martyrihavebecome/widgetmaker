import BaseComponent from "./BaseComponent";

export interface CloseProps { show?: boolean; }

// @data
export default class LpClose extends BaseComponent<CloseProps> {
    public static dataProp = "show";
    public cls = "LP-close-x";
    public render(h?: any) {
        return this.props.show !== false ? <div /> : null;
    }
}
