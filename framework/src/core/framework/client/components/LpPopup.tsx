import LpPage from "./LpPage";


// @data
export default class LpPopup extends LpPage<{ show?: boolean }> {
    public render(h?: any) {
        return <div class={"LP-popup LP-main" + (this.props.show ? " LP-show" : "")}>
            {this.props.children}
        </div>;
    }
}
