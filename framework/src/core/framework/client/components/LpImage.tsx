import BaseComponent from "./BaseComponent";


// @data
export default class LpImage extends BaseComponent<{ src: string }> {
    public static dataProp = "src";
    public render(h?: any) {
        if (this.props.src) {
            return <div class="LP-image" style={{ padding: "0px" }}>
                <img src={this.props.src} style={{ height: "210px" }}></img>
            </div>;
        } else {
            return null;
        }
    }
}
