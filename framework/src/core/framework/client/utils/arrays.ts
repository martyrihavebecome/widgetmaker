
export function remove<T>(arr: T[], item: T, all?: boolean): number {
    let result = arr.indexOf(item);
    while (result >= 0) {
        arr.splice(result, 1);
        result = all ? arr.indexOf(item) : -1;
    }
    return result;
}

export function findIndex<T>(arr: T[], callBack: (item: T, index?: number, arr?: T[]) => boolean): number {
    if (arr.findIndex) {
        return arr.findIndex(callBack);
    } else {
        const length = arr.length;
        for (let i = 0; i < length; i++) {
            if (callBack(arr[i], i, arr)) { return i; }
        }
    }
    return -1;
}



export function removeWhen<T>(arr: T[], callBack: (item: T, index?: number, arr?: T[]) => boolean, all?: boolean): number {
    let result = findIndex(arr, callBack);
    while (result >= 0) {
        arr.splice(result, 1);
        result = all ? findIndex(arr, callBack) : -1;
    }
    return result;
}

