/**
 * Взято из реакта
 * @link https://developmentarc.gitbooks.io/react-indepth/content/life_cycle/update/using_should_component_update.html
 */
export default function shallowEqual(objA: any, objB: any): boolean {
    if (objA === objB) {
        return true;
    }

    if (typeof objA !== "object" || objA === null ||
        typeof objB !== "object" || objB === null) {
        return false;
    }

    const keysA = Object.keys(objA);
    const keysB = Object.keys(objB);

    if (keysA.length !== keysB.length) {
        return false;
    }

    // Test for A's keys different from B.
    for (const i of keysA) {
        if (!objB.hasOwnProperty(i) || objA[i] !== objB[i]) {
            return false;
        }
    }

    return true;
}

