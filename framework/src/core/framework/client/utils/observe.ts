export default function observe(obj: any, onChange: (key: string, value: any) => void): void {
    for (const key of Object.keys(obj)) {
        let value = obj[key];
        if (typeof value === "object") {
            observe(value, (sub) => onChange(`${key}.${sub}`, value));
        }
        Object.defineProperty(obj, key, {
            get: () => value,
            set(newValue) {
                value = newValue;
                onChange(key, newValue);
            },
        });
    }
}
