import { removeWhen } from "./arrays";
export class Operation<T> {
    constructor(public data: T, public action: () => any) { }
}

// tslint:disable-next-line: max-classes-per-file
export default class Operations<T> {
    private operations: Array<Operation<T>> = [];
    public add(operation: Operation<T>, undo: (data: T) => boolean, all?: boolean): void {
        const operations = this.operations;
        removeWhen(operations, (op) => undo(op.data), all);
        this.operations.push(operation);
        operation.action();
    }
    public execute(): void {
        this.operations.forEach((action) => action.action());
    }
}
