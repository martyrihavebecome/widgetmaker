export function insert(parent: HTMLElement, child: HTMLElement, index: number) {
    const childNodes = parent.childNodes;
    if (index < childNodes.length) {
        parent.insertBefore(child, childNodes[index]);
    } else {
        parent.appendChild(child);
    }
}

export function stringToDom(str: string): Node {
    const parent = document.createElement("div");
    parent.innerHTML = str;
    return parent.firstChild;
}
