import Vue from 'vue';
import { BaseComponent } from '.';

declare global {
    function h(): any;
    namespace JSX {
        // tslint:disable no-empty-interface
        type Element = BaseComponent<any>;
        // tslint:disable no-empty-interface
        interface ElementClass { }
        interface IntrinsicElements {
            [elem: string]: any;
        }
    }
}
