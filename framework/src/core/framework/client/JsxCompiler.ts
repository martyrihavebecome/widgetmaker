import BaseComponent, { Props, defineProps } from "./components/BaseComponent";
import shallowEqual from "./utils/shallowEqual";
import { stringToDom } from "./utils/dom";
import * as components from "./";
import LpWidget from "./components/LpWidget";
import observe from "./utils/observe";


type Component = BaseComponent<Props>;


function insertAfter(parent: Node, lastNode: Node, el: Node): void {
    if (el) { parent.insertBefore(el, lastNode && lastNode.nextSibling || parent.firstChild); }
}

function convertStyle(obj: any): string {
    return typeof obj === "object" ? Object.keys(obj).map((key) => `${key.replace(/[A-Z]/, (val) => `-${val.toLocaleLowerCase()}`)}:${obj[key]}`).join(";") : obj;
}


export default class JsxCompiler {
    public static $createElement(cls: typeof BaseComponent | string, props: Props, children: any[], h?: any) {
        if (!children.length) {
            children = null as any;
        } else if (children[0] instanceof Array) {
            children = children[0];
        }
        // Переводим дочерние строки в компоненты
        // TODO: оптимизировать цикл
        children = children && children.map((child) => (child && typeof child !== "object") ? new BaseComponent({ text: child }) : child);
        props = defineProps(props || {}, { children });
        props.events = Object.keys(props)
            .filter((prop) => prop.startsWith("on") && typeof props[prop] === "function")
            .map((event) => ({ name: event.substr(2), fn: props[event] }));
        const result = (typeof cls === "string") ? new BaseComponent(props) : new cls(props);

        if (typeof cls === "string") {
            result.tag = cls; result.children = children;
        } else {
            // TODO: IE10
            result.tag = cls.name;
            children = result.render(result.$createElement);
            if (children) { children = [children]; }
            result.children = children;
        }
        return result;
    }

    public static update(source: Component, copy: Component) {
        // Если компонент вновь создан, или удален
        if (!copy || !source) {
            // Если компонент создан вновь, рендерим копию
            return copy === source;
        }
        const isSourceComponent = source.constructor !== BaseComponent;
        const isCopyComponent = copy.constructor !== BaseComponent;
        const sourceChildren = source.children || [];
        const copyChildren = copy.children || [];
        if (isSourceComponent || isCopyComponent) {
            return this.update(isSourceComponent ? sourceChildren[0] : source, isCopyComponent ? copyChildren[0] : copy);
        }
        const element = source.element;
        if (sourceChildren.length === copyChildren.length) {
            return copyChildren.some((copyItem, index) => {
                const sourceItem = sourceChildren[index];
                if (typeof sourceItem !== "object") { return sourceItem !== copyItem; }
                const sourceElement = sourceItem.element;
                const res = !shallowEqual(sourceItem.props, copyItem.props) || sourceItem.tag !== copyItem.tag || this.update(sourceItem, copyItem);
                let last: Node = null;
                if (res) {
                    sourceChildren.splice(index, 1, copyItem);
                    insertAfter(element, sourceElement || last, last = this.render(copyItem) || last);
                    if (sourceElement) { element.removeChild(sourceElement); }
                }
            });

        } else {
            return true;
        }
    }


    public static render(el: Component, parentProps?: Props): Node {
        const props = el.props;
        if (el && !el.tag) {
            return (el.element = document.createTextNode(props.text));
        }
        if (el && el instanceof components.Inner) {
            return (el.element = stringToDom(props.text));
        }
        if (el.constructor === BaseComponent) {
            const result = el.element = document.createElement(el.tag);
            const style = props.style;
            if (style) { props.style = convertStyle(style); }
            if (parentProps) {
                if (parentProps.class) { props.class = props.class ? `${props.class} ${parentProps.class}` : parentProps.class; }
                if (parentProps.events) { parentProps.events.forEach((event) => result.addEventListener(event.name, event.fn)); }
                if (parentProps.style) {
                    const parentStyle = convertStyle(parentProps.style);
                    props.style = props.style ? `${props.style};${parentStyle}` : parentStyle;
                }
            }
            props.events.forEach((event) => result.addEventListener(event.name, event.fn));
            if (props.inner) { result.innerHTML = props.inner; }
            Object.keys(props).forEach((prop: string) => {
                if (typeof props[prop] !== "function" && prop !== "events") {
                    result.setAttribute(prop, props[prop]);
                }
            });
            this.doRenderChildren(el).forEach((child) => child && result.appendChild(child));
            return result;
        } else {
            // TODO: Error length > 1
            if (el.cls) { props.class = props.class ? `${props.class} ${el.cls}` : el.cls; }
            const children = this.doRenderChildren(el, props);
            if (children && children.length) {
                return el.element = children[0] as any;
            }
            return null;
        }
    }

    public static doRenderChildren(el: Component, parentProps?: object): Array<Node | null> {
        return el.children ? el.children.map((child) => child ? this.render(child, parentProps) : null) : [];
    }

    public static compile(pages: components.Page[], jsCode: string): { widget: LpWidget, context: object } {
        const context = {};
        const code = new Function("LpTool", jsCode);
        code(context);
        const widget = this.doCompile(pages, context);
        observe(context, () => this.updateWithErrorHandle(widget, pages, context));
        return { widget, context };
    }

    public static updateWithErrorHandle(source: Component, pages: components.Page[], context?: object): void {
        let copy!: Component;
        try {
            copy = this.doCompile(pages, context);
        } catch (e) {
            console.log("Ошибка в коде");
            return;
        }
        if (copy) { this.update(source, copy); }
    }

    public static doCompile(pages: components.Page[], context: object): LpWidget {
        const widget = new LpWidget(pages, { ...components, LPTool: context, LpSystem: { show: "" } });
        const root = widget.render(widget.$createElement);
        widget.children = [root];
        return widget;
    }
}
