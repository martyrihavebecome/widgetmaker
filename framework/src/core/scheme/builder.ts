import components from "./components";
import LpWidget from "../framework/client/components/LpWidget";
import LpPage from "../framework/client/components/LpPage";
import Variable, { Scheme } from "@/utils/config/variable";
import { BaseComponent } from "../framework/client";

export interface Config {
    scheme: Scheme;
    pageId: string;
}

export default class Builder {
    private static resolvePropType(className: string, key: string, title: string): object {
        const clsScheme = components[className.toLowerCase()];
        if (clsScheme && clsScheme[key]) {
            return { ...clsScheme[key], title };
        } else {
            return { title };
        }
    }

    private cache: { [pageId: string]: { [name: string]: Variable } } = Object.create(null);
    public compiled(widget: LpWidget): Config[] {
        const popups = widget.find((page) => page instanceof LpPage);
        return popups.map((popup) => {
            const pageId = popup.props.pageId || "";
            const cache = this.cache[pageId] || (this.cache[pageId] = Object.create(null));
            let scheme: Scheme = [];
            popup.foreach((cmp) => {
                const cls = cmp.constructor as typeof BaseComponent;
                const props = cmp.props;
                const clsName = cls.name;
                const keys: Array<[string, string]> = [];
                if (props.data && cls.dataProp) {
                    keys.push([cls.dataProp, props.data]);
                }
                Object.keys(props).forEach((key) => {
                    const value = props[key];
                    if (typeof value === "string" && value.startsWith("@")) {
                        keys.push([key, value.substr(1)]);
                    }
                });
                scheme = scheme.concat(keys.map((keyValue) => cache[keyValue[1]]
                    || (cache[keyValue[1]] = { ...new Variable(keyValue[1]), ...Builder.resolvePropType(clsName, keyValue[0], keyValue[1]) })));
            });
            return { pageId, scheme };
        });
    }

}
