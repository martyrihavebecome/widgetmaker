export interface ISchemeBase {
    id?: string;
    name?: string;
    type?: string;
    title?: string;
    default?: string;
    group?: string;
    order?: number;
    fieldType?: string;
    values: string | string[];
    weight?: number;
}

interface Hash<T> { [field: string]: T; }

export interface IObjectProperty extends ISchemeBase {
    properties?: Hash<IObjectProperty | IProperty>;
    definitions?: object;
    required?: string[];
}

export interface IProperty extends ISchemeBase {
    examples?: [];
    media?: object;
    items?: IObjectProperty | IProperty;
}

export const components = {
    lppopup: {
        position: {
            type: "string",
            enum: ["lefttop", "center", "rightbottom"],
        },

    },
    lpimage: {
        src: {
            type: "string",
            /*media: {
                type: "image/jpeg",
                binaryEncoding: "base64",
            },*/
            // fieldType: "image",
        },

    },
    lphtml: {
        text: {
            type: "string",
        },
    },
    lpform: {
        fields: {
            type: "form",
            default: "model.form",
        },
    },
    lpfield: {
        name: { type: "string", reqired: true },
        placeholder: { type: "string" },
    },
    lpclose: {
        show: { type: "boolean", default: true },
    },
    lpnote: {
        data: { type: "string" },
    },
};

export default components;
