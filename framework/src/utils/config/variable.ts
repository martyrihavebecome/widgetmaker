export default class Variable {
    public type?: string;
    public step?: number;
    public title?: string;
    public desc?: string;
    public default?: string;
    public group?: string;
    public order?: number;
    public fieldType?: string;
    constructor(public name: string) { }
}

export type Scheme = Variable[];
