export default class Event<Target extends EventTarget, Name extends keyof Target> {
    constructor(public target: Target, public name: Name) {

    }
}
